//
//  ManVideoController.swift
//  Manplus_Sample_Swift
//
//  Created by MezzoMedia on 20/08/2019.
//  Copyright © 2019 MezzoMedia. All rights reserved.
//

import Foundation
import WebKit

class ManVideoController: UIViewController, ManVideoDelegate {
    
    var manVideo: ManVideo?
    var publisherID = ""
    var mediaID = ""
    var sectionID = ""
    var i_x: CGFloat = 0.0
    var i_y: CGFloat = 0.0
    var i_width: CGFloat = 0.0
    var i_height: CGFloat = 0.0

    var appID = ""
    var appName = ""
    var storeURL = ""
    
    var autoplay = false        //AutoPlay video ad
    var autoReplay = false      //Auto-replay video ad
    var muted = false           //Play video ad with muted
    var clickFull = false       //Set click area to land on ad details page
    var viewability = false     //Stop the video if the ad area appears to be less than 20% when scrolling through the page
    var closeBtnShow = false    //Close button clears ad area
    var soundBtnShow = false    //Button to control sound
    var clickBtnShow = false    //Ad click button
    var skipBtnShow = false     //Ad skip button
    var clickVideoArea = false  //Video area disappears when clicked

    func prefersStatusBarHidden() -> Bool {
        
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Video AD"
        view.backgroundColor = UIColor.white
        
        manVideo = ManVideo(frame: CGRect(x: i_x, y: i_y, width: i_width, height: i_height))
        
        manVideo?.infoSDK()

        manVideo?.gender = "2"
        manVideo?.age = "15"
        manVideo?.userId = "mezzomedia"
        manVideo?.userEmail = "mezzo@mezzomeida.co.kr"
        manVideo?.userPositionAgree = "1"
        
        manVideo?.videoDelegate = self

        // u_age_level (0: 만13세 이하, 1: 만13세 이상)<필수 Parameter>
        manVideo?.userAgeLevel("1")
        
        // app_id, app_name, stroe_url 설정
        manVideo?.appID(appID, appName: appName, storeURL: storeURL)
        
        // 동영상 광고의 옵션 설정
        manVideo?.autoplay(autoplay, autoReplay: autoReplay, muted: muted, clickFull: clickFull, closeBtnShow: closeBtnShow, soundBtnShow: soundBtnShow, clickBtnShow: clickBtnShow, skipBtnShow: skipBtnShow, clickVideoArea: clickBtnShow, viewability: viewability)
        manVideo?.publisherID(publisherID, mediaID: mediaID, sectionID: sectionID, x: i_x, y: i_y, width: i_width, height: i_height)

        // Keyword 타게팅을 위한 함수파라미터 (Optional)
        manVideo?.keywordParam("KeywordTargeting")
        
        manVideo?.externalParam("VideoAdditionalParameters")
        
        view.addSubview(manVideo!)
        
        manVideo?.start()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
        print("ADVideo ================== didReceiveMemoryWarning")
    }
    
    func setPID(_ p: String?, mid m: String?, sid s: String?, x: CGFloat, y: CGFloat, width w: CGFloat, height h: CGFloat) {
        publisherID = p ?? ""
        mediaID = m ?? ""
        sectionID = s ?? ""
        i_x = x
        i_y = y
        i_width = w
        i_height = h
        
        print("ADVideo ================== Publisher, Media, Section NO")
        print("\(publisherID)")
        print("\(mediaID)")
        print("\(sectionID)")
        print("\(i_x)")
        print("\(i_y)")
        print("\(i_width)")
        print("\(i_height)")
        print("======================================================")
        
    }
    
    
    func setAppID(_ app_id: String?, appName app_name: String?, storeURL store_url: String?) {
        appID = app_id ?? ""
        appName = app_name ?? ""
        storeURL = store_url ?? ""
        
        print("ADVideo ================== appID, appName, storeURL")
        print("\(appID)")
        print("\(appName)")
        print("\(storeURL)")
        print("======================================================")
        
    }
    
    func setAutoplay(_ _autoplay: Bool, autoReplay _autoReplay: Bool, muted _muted: Bool, clickFull _clickFull: Bool, closeBtnShow _closeBtnShow: Bool, soundBtnShow _soundBtnShow: Bool, clickBtnShow _clickBtnShow: Bool, skipBtnShow _skipBtnShow: Bool, clickVideoArea _clickVideoArea: Bool, viewability _viewability: Bool) {
        autoplay = _autoplay
        autoReplay = _autoReplay
        muted = _muted
        clickFull = _clickFull
        viewability = _viewability
        closeBtnShow = _closeBtnShow
        soundBtnShow = _soundBtnShow
        clickBtnShow = _clickBtnShow
        skipBtnShow = _skipBtnShow
        clickVideoArea = _clickVideoArea
        
        print("ADVideo ================== autoplay, autoReplay, muted, clickFull, closeBtnShow, soundBtnShow, clickBtnShow, skipBtnShow")
        print("Autoplay : \(autoplay ? "TRUE" : "FALSE")")
        print("AutoReplay : \(autoReplay ? "TRUE" : "FALSE")")
        print("Muted : \(muted ? "TRUE" : "FALSE")")
        print("ClickFull : \(clickFull ? "TRUE" : "FALSE")")
        print("Viewability : \(viewability ? "TRUE" : "FALSE")")
        print("CloseBtnShow : \(closeBtnShow ? "TRUE" : "FALSE")")
        print("SoundBtnShow : \(soundBtnShow ? "TRUE" : "FALSE")")
        print("ClickBtnShow : \(clickBtnShow ? "TRUE" : "FALSE")")
        print("SkipBtnShow : \(skipBtnShow ? "TRUE" : "FALSE")")
        print("ClickVideoArea : \(clickVideoArea ? "TRUE" : "FALSE")")
        print("======================================================")
        
    }
    
    func didFailReceiveAd(_ adVideo: ManVideo?, errorType: Int) {
        
        // errorType은 ManAdDefine.h 참조
        print(String(format: "[ManVideoAdViewController] =========== didFailReceiveAd : %ld", errorType))
                var log = "none"
                switch errorType {
                case Int(NewManAdSuccess.rawValue):
                    log = "성공"
                case Int(NewManAdClick.rawValue):
                    log = "광고 클릭"
                case Int(NewManAdClose.rawValue):
                    log = "광고 닫음";
                case Int(NewManVideoAdStart.rawValue):
                    log = "비디오 광고 시작";
                case Int(NewManVideoAdSkip.rawValue):
                    log = "비디오 광고 Skip";
                case Int(NewManVideoAdImp.rawValue):
                    log = "비디오 노출"
                case Int(NewManVideoAdFirstQ.rawValue):
                    log = "비디오 1/4 재생"
                case Int(NewManVideoAdMidQ.rawValue):
                    log = "비디오 1/2 재생"
                case Int(NewManVideoAdThirdQ.rawValue):
                    log = "비디오 3/4 재생"
                case Int(NewManVideoAdComplete.rawValue):
                    log = "비디오 광고 재생 완료";
                case Int(NewManAdNotError.rawValue):
                    log = "광고 없음(No Ads)"
                case Int(NewManAdPassbackError.rawValue):
                    log = "Sync 모드 필요 (패스백)"
                case Int(NewManAdTimeoutError.rawValue):
                    log = "Timeout"
                case Int(NewManAdParsingError.rawValue):
                    log = "파싱 에러"
                case Int(NewManAdDuplicateError.rawValue):
                    log = "중복 호출 에러"
                case Int(NewManAdError.rawValue):
                    log = "에러"
                case Int(NewManBrowserError.rawValue):
                    log = "Browser Error"
                case Int(NewManAdNotExistError.rawValue):
                    log = "존재하지 않는 요청 에러"
                case Int(NewManAdAppStoreUrlError.rawValue):
                    log = "앱 Store URL 미입력"
                case Int(NewManAdIDError.rawValue):
                    log = "사업자/미디어/섹션 코드 미존재"
                case Int(NewManAdReloadTimeError.rawValue):
                    log = "광고 재호출(Reload) 에러"
                case Int(NewManAdNetworkError.rawValue):
                    log = "네트워크 에러"
                case Int(NewManAdFileError.rawValue):
                    log = "광고물 파일 형식 에러"
                case Int(NewManAdCreativeError.rawValue):
                    log = "광고물 요청 실패(Timeout)"
                default:
                    break
                }
        
                log = log + (String(format: " : %ld", errorType))
        
                navigationController?.view.makeToast(log)
    }
    
    // 지정된 주기 이내에 광고리로드가 발생됨
    func didBlockReloadAd(_ adVideo: ManVideo?) {
        print("[ManVideoAdViewController] =========== didBlockReloadAd")
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        if toInterfaceOrientation == .portrait || toInterfaceOrientation == .portraitUpsideDown {
            print("PORTRAIT")
            var frame = manVideo?.bounds
            frame?.origin.x = i_x
            frame?.origin.y = i_y
            frame?.size.width = i_width
            frame?.size.height = i_height
            manVideo?.bounds = frame!
            manVideo?.frame = frame!
        } else if toInterfaceOrientation == .landscapeLeft || toInterfaceOrientation == .landscapeRight {
            print("LANDSCAPE")
            let screen = UIScreen.main.bounds
            var frame = manVideo?.bounds
            frame?.origin.x = 0
            frame?.origin.y = 0
            frame?.size.width = screen.size.height
            frame?.size.height = screen.size.width
            manVideo?.bounds = frame!
            manVideo?.frame = frame!
        }
    }

}
