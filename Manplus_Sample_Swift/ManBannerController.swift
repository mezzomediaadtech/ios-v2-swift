//
//  ManBannerController.swift
//  Manplus_Sample_Swift
//
//  Created by MezzoMedia on 20/08/2019.
//  Copyright © 2019 MezzoMedia. All rights reserved.
//

import Foundation
import WebKit

class ManBannerController: UIViewController, ManBannerDelegate {

    var manBanner: ManBanner?
    var publisherID = ""
    var mediaID = ""
    var sectionID = ""
    var i_x: CGFloat = 0.0
    var i_y: CGFloat = 0.0
    var i_width: CGFloat = 0.0
    var i_height: CGFloat = 0.0
    var bannerType = ""
    var appID = ""
    var appName = ""
    var storeURL = ""

    func prefersStatusBarHidden() -> Bool {
        
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Banner AD"
        view.backgroundColor = UIColor.white
        
        manBanner = ManBanner(frame: CGRect(x: i_x, y: i_y, width: i_width, height: i_height))
        
        manBanner?.infoSDK()
        
        manBanner?.gender = "2"
        manBanner?.age = "15"
        manBanner?.userId = "mezzomedia"
        manBanner?.userEmail = "mezzo@mezzomeida.co.kr"
        manBanner?.userPositionAgree = "1"
        
        manBanner?.bannerDelegate = self
        
        // u_age_level (0: 만13세 이하, 1: 만13세 이상)<필수 Parameter>
        manBanner?.userAgeLevel("1")
        
        // app_id, app_name, stroe_url 설정
        manBanner?.appID(appID, appName: appName, storeURL: storeURL)
        
        manBanner?.publisherID(publisherID, mediaID: mediaID, sectionID: sectionID, x: i_x, y: i_y, width: i_width, height: i_height, type: bannerType)

        // Keyword 타게팅을 위한 함수파라미터 (Optional)
        manBanner?.keywordParam("KeywordTargeting")
        
        manBanner?.externalParam("BannerAdditionalParameters")

        view.addSubview(manBanner!)

        manBanner?.start()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
        print("ADBanner ================== didReceiveMemoryWarning")
    }
    
    func setPID(_ p: String?, mid m: String?, sid s: String?, x: CGFloat, y: CGFloat, width w: CGFloat, height h: CGFloat, type t: String?) {
        publisherID = p ?? ""
        mediaID = m ?? ""
        sectionID = s ?? ""
        i_x = x
        i_y = y
        i_width = w
        i_height = h
        bannerType = t ?? ""

        print("ADBanner ================== Publisher, Media, Section NO")
        print("\(publisherID)")
        print("\(mediaID)")
        print("\(sectionID)")
        print("\(i_x)")
        print("\(i_y)")
        print("\(i_width)")
        print("\(i_height)")
        print("\(bannerType)")
        print("======================================================")

    }


    func setAppID(_ app_id: String?, appName app_name: String?, storeURL store_url: String?) {
        appID = app_id ?? ""
        appName = app_name ?? ""
        storeURL = store_url ?? ""

        print("ADBanner ================== appID, appName, storeURL")
        print("\(appID)")
        print("\(appName)")
        print("\(storeURL)")
        print("======================================================")

    }
    
    func didFailReceiveAd(_ adBanner: ManBanner?, errorType: Int) {
        
        // errorType은 ManAdDefine.h 참조
        print(String(format: "[ManBannerAdViewController] =========== didFailReceiveAd : %ld", errorType))
        var log = "none"
        switch errorType {
        case Int(NewManAdSuccess.rawValue):
            log = "성공"
        case Int(NewManAdClick.rawValue):
            log = "광고 클릭"
        case Int(NewManAdNotError.rawValue):
            log = "광고 없음(No Ads)"
        case Int(NewManAdPassbackError.rawValue):
            log = "Sync 모드 필요 (패스백)"
        case Int(NewManAdTimeoutError.rawValue):
            log = "Timeout"
        case Int(NewManAdParsingError.rawValue):
            log = "파싱 에러"
        case Int(NewManAdDuplicateError.rawValue):
            log = "중복 호출 에러"
        case Int(NewManAdError.rawValue):
            log = "에러"
        case Int(NewManBrowserError.rawValue):
            log = "Browser Error"
        case Int(NewManAdNotExistError.rawValue):
            log = "존재하지 않는 요청 에러"
        case Int(NewManAdAppStoreUrlError.rawValue):
            log = "앱 Store URL 미입력"
        case Int(NewManAdIDError.rawValue):
            log = "사업자/미디어/섹션 코드 미존재"
        case Int(NewManAdTargetAreaError.rawValue):
            log = "광고영역크기 에러"
        case Int(NewManAdReloadTimeError.rawValue):
            log = "광고 재호출(Reload) 에러"
        case Int(NewManAdNetworkError.rawValue):
            log = "네트워크 에러"
        case Int(NewManAdFileError.rawValue):
            log = "광고물 파일 형식 에러"
        case Int(NewManAdCreativeError.rawValue):
            log = "광고물 요청 실패(Timeout)"
        default:
            break
        }

        log = log + (String(format: " : %ld", errorType))

        navigationController?.view.makeToast(log)
    }
    
    // 지정된 주기 이내에 광고리로드가 발생됨
    func didBlockReloadAd(_ adBanner: ManBanner?) {
        print("[ManBannerAdViewController] =========== didBlockReloadAd")
    }
}
